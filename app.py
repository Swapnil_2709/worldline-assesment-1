from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)


def load_tasks():
    try:
        with open('tasks.txt', 'r') as f:
            return [line.strip() for line in f.readlines()]
    except FileNotFoundError:
        return []


def save_tasks(tasks):
    with open('tasks.txt', 'w') as f:
        for task in tasks:
            f.write(task + '\n')


tasks = load_tasks()


@app.route('/')
def index():
    return render_template('index.html', tasks=tasks)

@app.route('/add', methods=['POST'])
def add_task():
    task_content = request.form['content']
    tasks.append(task_content)
    save_tasks(tasks)  
    return redirect(url_for('index'))

@app.route('/delete/<int:index>')
def delete_task(index):
    del tasks[index]
    save_tasks(tasks) 
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)
